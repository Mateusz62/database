package components.database;

import components.database.interfaces.IUserData;
import components.database.interfaces.IActivateUserData;
import components.database.interfaces.IActivationCodesData;
import components.database.interfaces.ICommonData;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Mateusz on 2015-01-04.
 */
public class Database
        implements IActivateUserData, IActivationCodesData, ICommonData, IUserData  {

    private Connection connection = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private PreparedStatement preparedSTatement = null;


    /**
     * Dependecy injection.
     * @param connection Connection to database.
     */
    public Database(Connection connection) {
        this.connection = connection;
        try{
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // resultSet gets the result of the SQL query
    }

    /**
     * Clean up database.
     */
    public void close() {
        try {
            connection.close();
            statement.close();
            resultSet.close();
            preparedSTatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Checks if user is activated.
     * @param username Username.
     * @return True if user is activated.
     */
    @Override
    public boolean isUserActive(String username) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT activation FROM fanzol.members WHERE name = ?");
            preparedSTatement.setString(1, username);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next())
                return resultSet.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Gets user's email.
     * @param username Username.
     * @return User's email.
     */
    @Override
    public String getUserEmail(String username) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT email FROM fanzol.members WHERE name = ?");
            preparedSTatement.setString(1, username);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next())
                return resultSet.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * =activateUser();
     * @param username User to activate.
     * @return True if user was activated.
     */
    @Override
    public boolean activate(String username) {
        return activateUser(username);
    }

    /**
     * Gets activation code for a given user.
     * @param username Username.
     * @return Activation code.
     */
    @Override
    public String getActivationCode(String username) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT activation_code FROM fanzol.members WHERE name = ?");
            preparedSTatement.setString(1, username);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next()) {
                String code = resultSet.getString(1);
                if(code.equals("none"))
                    return null;
                else
                    return code;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sets activation code for a given user.
     * @param username Username.
     * @param code Activation code.
     * @return True if adding code succeeded.
     */
    @Override
    public boolean setActivationCode(String username, String code) {
        try {
            if(!userExists(username))
                return false;
            preparedSTatement = connection.prepareStatement("UPDATE fanzol.members SET activation_code=? WHERE name=?");
            preparedSTatement.setString(1, code);
            preparedSTatement.setString(2, username);
            preparedSTatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }


    /**
     * Sets password of a user.
     * @param username User for which to change password.
     * @param password New password.
     * @return True if password was changed.
     */
    @Override
    public boolean setPassword(String username, String password) {
        try {
            if(!userExists(username))
                return false;
            preparedSTatement = connection.prepareStatement("UPDATE fanzol.members SET pass=? WHERE name=?");
            preparedSTatement.setString(1, password);
            preparedSTatement.setString(2, username);
            preparedSTatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Activates user.
     * @param username User to activate.
     * @return True if everything went fine.
     */
    @Override
    public boolean activateUser(String username) {
        try {
            if(!userExists(username))
                return false;
            preparedSTatement = connection.prepareStatement("UPDATE fanzol.members SET activation=1 WHERE name=?");
            preparedSTatement.setString(1, username);
            preparedSTatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Gets path to an avatar for a given user.
     * @param username Username.
     * @return Path to avatar.
     */
    @Override
    public String getAvatar(String username) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT avatar FROM fanzol.members WHERE name = ?");
            preparedSTatement.setString(1, username);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next())
                return resultSet.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sets path to an avatar.
     * @param username Username.
     * @param avatar Path to avatar.
     * @return True if adding path succeeded.
     */
    @Override
    public boolean setAvatar(String username, String avatar) {
        try {
            if(!userExists(username))
                return false;
            preparedSTatement = connection.prepareStatement("UPDATE fanzol.members SET avatar=? WHERE name=?");
            preparedSTatement.setString(1, avatar);
            preparedSTatement.setString(2, username);
            preparedSTatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Deactivates user.
     * @param username User to deactivate.
     * @return True if everything went fine.
     */
    @Override
    public boolean deactivateUser(String username) {
        try {
            if(!userExists(username))
                return false;
            preparedSTatement = connection.prepareStatement("UPDATE fanzol.members SET activation=0 WHERE name=?");
            preparedSTatement.setString(1, username);
            preparedSTatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Returns users contacts.
     * @param username User wich contacts we want.
     * @return Return String[] of contacts, or null otherwise.
     */
    @Override
    public String[] getUserContacts(String username) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT user2 FROM fanzol.friends WHERE user1 = ?");
            preparedSTatement.setInt(1, getUserId(username));
            resultSet = preparedSTatement.executeQuery();
            ArrayList<String> contacts = new ArrayList<String>();

            while(resultSet.next()) {
                contacts.add(getNameById(resultSet.getInt(1)));
            }
            return contacts.toArray(new String[contacts.size()]);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Adds contact to username's list.
     * @param username User that wants to add contact.
     * @param friend Contact to add.
     * @return True if adding succeeded.
     */
    @Override
    public boolean addContact(String username, String friend) {
        try {
            if(!userExists(username) || !userExists(friend))
                return false;
            // Get id's.
            int id1 = getUserId(username);
            int id2 = getUserId(friend);

            // Is id2 already friend of id1?
            if(isFriendOf(id1, id2))
                return false;

            preparedSTatement = connection.prepareStatement("INSERT INTO fanzol.friends VALUES (?, ?)");
            preparedSTatement.setInt(1, id1);
            preparedSTatement.setInt(2, id2);
            preparedSTatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Checks if user exists.
     * @param userName Username to test for.
     * @return True if user with give username already exists.
     */
    @Override
    public boolean userExists(String userName) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT id FROM fanzol.members WHERE name = ?");
            preparedSTatement.setString(1, userName);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next())
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Checs whether given email is already used.
     * @param email Email to test for.
     * @return True if email is already in use.
     */
    @Override
    public boolean emailUsed(String email) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT id FROM fanzol.members WHERE email = ?");
            preparedSTatement.setString(1, email);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next())
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Gets User by session key.
     * @param sessionKey Session key.
     * @return User.
     */
    @Override
    public User getUserBySession(String sessionKey) {
        try {
            renewSession(sessionKey);

            preparedSTatement = connection.prepareStatement("SELECT user_id from fanzol.sessions WHERE skey=?");
            preparedSTatement.setString(1, sessionKey);
            resultSet = preparedSTatement.executeQuery();
            if (resultSet.next())
                return getUserByName(getNameById(resultSet.getInt(1)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Gets user from database.
     * @param userName User to get.
     * @return User if exists.
     */
    @Override
    public User getUserByName(String userName) {
        Boolean logged = isUserAlreadyLoggedIn(userName);
        User user = null;
        try {
            if (!userExists(userName))
                return null;
            preparedSTatement = connection.prepareStatement("SELECT pass, activation FROM fanzol.members WHERE name = ?");
            preparedSTatement.setString(1, userName);
            resultSet = preparedSTatement.executeQuery();
            if (resultSet.next()) {
                ArrayList<GrantedAuthority> authorityCollection = new ArrayList<GrantedAuthority>();
                user = new User(userName, resultSet.getString(1), logged, resultSet.getBoolean(2), true, true, authorityCollection);
            }
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Gets session last activity date.
     * @param sessionKey Session key.
     * @return java.util.Date.
     */
    @Override
    public Date getSessionBeginDate(String sessionKey) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT lasts from fanzol.sessions WHERE skey = ?");
            preparedSTatement.setString(1, sessionKey);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next()) {
                Date d = new Date(resultSet.getDate(1).getTime());
                return d;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Removes session from database.
     * @param sessionKey Session key.
     * @return True removing succeeded.
     */
    @Override
    public boolean removeSession(String sessionKey) {
        try {
            if(!sessionExists(sessionKey))
                return false;

            preparedSTatement = connection.prepareStatement("DELETE FROM fanzol.sessions WHERE skey=?");
            preparedSTatement.setString(1, sessionKey);
            preparedSTatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Add session for given user and session key.
     * @param sessionKey Session key.
     * @param userName Username.
     * @return True if session was added to database.
     */
    @Override
    public boolean addSession(String sessionKey, String userName) {
        try {
            if(!userExists(userName) || sessionExists(sessionKey))
                return false;

            String query = String.format("INSERT INTO fanzol.sessions VALUES ('%d', '%s', ?)", getUserId(userName), sessionKey);

            preparedSTatement = connection.prepareStatement(query);
            preparedSTatement.setDate(1, new java.sql.Date(new Date().getTime()));

            preparedSTatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Add user to database.
     * @param userName Username.
     * @param password Password, not hashed.
     * @param email Email.
     * @param args  args[0] - not used in communicator.
     * @return True if adding succeeded.
     */
    @Override
    public boolean addUser(String userName, String password, String email, String[] args) {
        try {
            if(userExists(userName))
                return false;
            preparedSTatement = connection.prepareStatement("INSERT INTO fanzol.members VALUES (DEFAULT, ?, ?, ?, DEFAULT, DEFAULT, DEFAULT)");
            preparedSTatement.setString(1, userName);
            preparedSTatement.setString(2, email);
            preparedSTatement.setString(3, password);
            preparedSTatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Gets user's id.
     * @param username User for which to get id.
     * @return Returns user's id.
     */
    private int getUserId(String username) {
        try {
            if(!userExists(username))
                return 0;
            preparedSTatement = connection.prepareStatement("SELECT id from fanzol.members WHERE name=?");
            preparedSTatement.setString(1, username);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next())
                return resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Gets users name by id.
     * @param id Id for which to get username.
     * @return Returns username of a id.
     */
    private String getNameById(Integer id) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT name from fanzol.members WHERE id=?");
            preparedSTatement.setInt(1, id);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next())
                return resultSet.getString(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Checks if user has friend in his contact list.
     * @param user User.
     * @param friend Friend of user.
     * @return True if user has friend in his contact list.
     */
    private Boolean isFriendOf(int user, int friend) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT user1 FROM fanzol.friends WHERE user1 = ? AND user2 = ?");
            preparedSTatement.setInt(1, user);
            preparedSTatement.setInt(2, friend);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next())
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Checks if there is session with given session key.
     * @param key Session key.
     * @return True if there is session registered with given session key.
     */
    private Boolean sessionExists(String key) {
        try {
            preparedSTatement = connection.prepareStatement("SELECT user_id FROM fanzol.sessions WHERE skey = ?");
            preparedSTatement.setString(1, key);
            resultSet = preparedSTatement.executeQuery();
            if(resultSet.next())
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Checks whether there is already session created for given username.
     * @param username Username.
     * @return True if there is session registered for given username.
     */
    private Boolean isUserAlreadyLoggedIn(String username) {
        try {
            Integer id = getUserId(username);
            preparedSTatement = connection.prepareStatement("SELECT user_id FROM fanzol.sessions WHERE user_id = ?");
            preparedSTatement.setInt(1, id);
            resultSet = preparedSTatement.executeQuery();
            if (resultSet.next())
                return resultSet.getInt(1) > 0 ? true : false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Renews session, sets lasts(last activity) field to current time.
     * @param sessionKey Session key.
     * @return True if renew succeeded.
     */
    private Boolean renewSession(String sessionKey) {
        try {
            preparedSTatement = connection.prepareStatement("UPDATE fanzol.sessions SET lasts=? WHERE skey=?");
            preparedSTatement.setDate(1,  new java.sql.Date(new Date().getTime()));
            preparedSTatement.setString(2, sessionKey);
            preparedSTatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
}
