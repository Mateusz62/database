package components.database.interfaces;

/**
 * Created by Mateusz on 2015-01-04.
 */
public interface IActivationCodesData {
    String getActivationCode(String username);
    boolean setActivationCode(String username, String code);
}
