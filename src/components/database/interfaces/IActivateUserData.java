package components.database.interfaces;

/**
 * Created by Mateusz on 2015-01-04.
 */
public interface IActivateUserData {
    boolean isUserActive(String username);
    String getUserEmail(String username);
    boolean activate(String username);
}
