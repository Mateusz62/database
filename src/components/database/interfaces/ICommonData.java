package components.database.interfaces;

/**
 * Created by Mateusz on 2015-01-04.
 */
public interface ICommonData {
    boolean setPassword(String username, String password);
    boolean activateUser(String username);
    String getAvatar(String username);
    boolean setAvatar(String username, String avatar);
    boolean deactivateUser(String username);
    String[] getUserContacts(String username);
    boolean addContact(String username, String friend);
}
